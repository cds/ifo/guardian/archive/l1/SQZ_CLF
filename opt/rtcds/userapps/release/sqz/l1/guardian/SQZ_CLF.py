# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_CLF.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_CLF.py $

from guardian import GuardState, GuardStateDecorator
from guardian.state import TimerManager
import time
from noWaitServo import Servo
import numpy as np

from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

# this is to keep emacs python-mode happy from lint errors due
# to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    notify = None
    log = None

if sqzconfig.nosqz == True:
    nominal = 'IDLE'
#    nominal = 'CLF_REFL_LOCKED'
else:
    nominal = 'CLF_REFL_LOCKED'
    #nominal = 'LOCKED'

class CLFParams(EzcaUser):
    # counter for lock attempts
    LOCK_ATTEMPT_COUNTER = 0
    LOCK_ATTEMPT_MAX = 5

    FAULT_TIMEOUT_S        = 1
    UNLOCK_TIMEOUT_S       = 1
    REFL_TIMEOUT_S       = 1

    # these rails are for the IFR input
    # can change to +-10 when usin VCXO
    CMB_OUT_RAIL_HIGH = 5
    CMB_OUT_RAIL_LOW  = -10

    AVG_COUNT = 3

    intervention_msg = None

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0
        self.timer['REFL_TIMEOUT'] = 0

        


    #TODO, change these to exponential averaging
    refl_DC_powers = []
    refl_RF_levels = []
    def fault_checker(self):
        fault_condition = False
        return fault_condition

    def REFL_checker(self):
        return ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'] > ezca['SQZ-CLF_GRD_RF_MIN']

    def OPO_CLFlock_checker(self):
        return ezca['GRD-SQZ_OPO_STATE_S'] == 'CLF_DITHER_LOCKED'

    def check_unlocked(self):
        unlock_condition = False

        # if the output is railed, we are not locked
        if ezca['SQZ-CLF_SERVO_SLOWMON'] < self.CMB_OUT_RAIL_LOW:
            notify("CMB OUT RAILED LOW")
            notify('%d'%(ezca['SQZ-CLF_SERVO_SLOWMON']))
            unlock_condition = True

        if ezca['SQZ-CLF_SERVO_SLOWMON'] > self.CMB_OUT_RAIL_HIGH:
            notify("CMB OUT RAILED HIGH")
            unlock_condition = True

        return unlock_condition


class CLFShared(SharedState):
    @DecoShared
    def clf(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return CLFParams()

shared = CLFShared()


#############################################
#Functions
# reset everything to the good values for acquisition
def down_state_set():
    # disable dither
    ezca['SQZ-ASC_WFS_GAIN'] = 0
    for dof in ['PIT1','PIT2','YAW1','YAW2']:
        ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = 0

    ezca['SQZ-PHASE_DITHER_OSC_CLKGAIN'] = 0

    ezca['SQZ-CLF_SERVO_IN1EN'] = False
    ezca['SQZ-CLF_SERVO_IN2EN'] = False
    ezca['SQZ-CLF_SERVO_COMEXCEN'] = True
    ezca['SQZ-CLF_SERVO_SLOWEXCEN'] = False
    ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
    ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 0
    ezca['SQZ-OPO_TEC_LOCKIN_SWITCH'] = 'Off'
    ezca['SQZ-CLF_SERVO_DC_OFFSET'] = 0

    ezca.switch('SQZ-PHASE_LOCK','INPUT','OFF')
    ezca['SQZ-PHASE_LOCK_RSET'] = 2

    for ii in range(3):
        ezca['SQZ-CLF_SERVO_DC_INMTRX_1_%s'%(ii+1)] = 0
    ezca.switch('SQZ-CLF_SERVO_DC','INPUT','OUTPUT','ON','OFFSET','FM1','FM2','FM3','FM4','FM5','FM6','FM7','FM8','FM9','FM10','OFF')
    ezca['SQZ-CLF_SERVO_DC_GAIN'] = 1




#############################################
#Decorator

# There should be at least two types of fault_checker here.
# One you go back to trying again (lock loss), another you just go straight to down
# (no laser beam).

class lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.clf.check_unlocked():
            if shared.clf.timer['UNLOCK_TIMEOUT']:
                return 'LOCKLOSS'
        else:
            shared.clf.timer['UNLOCK_TIMEOUT'] = shared.clf.UNLOCK_TIMEOUT_S


class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.clf.fault_checker():
            if shared.clf.timer['FAULT_TIMEOUT']:
                return 'FAULT'
        else:
            shared.clf.timer['FAULT_TIMEOUT'] = shared.clf.FAULT_TIMEOUT_S

class REFL_checker(GuardStateDecorator):
    def pre_exec(self):
        if not shared.clf.REFL_checker():
            if shared.clf.timer['REFL_TIMEOUT']:
                return 'DOWN'
        else:
            shared.clf.timer['REFL_TIMEOUT'] = shared.clf.REFL_TIMEOUT_S

class CLF_DITHER_LOCK_checker(GuardStateDecorator):
    def pre_exc(self):
        if not shared.clf.OPO_CLFlock_checker():
            return 'DOWN'
#############################################
#States
class FAULT(GuardState):
    index   = 2
    redirect = False
    request = False

    def main(self):
        down_state_set()

    def run(self):
        notify("Try Press INIT in SQZ_CLF Guardian.")
        blocked = shared.clf.fault_checker()
        if blocked is None:
            return
        if not blocked:
            return True

class DOWN(GuardState):
    index   = 1
    request = False
    goto    = True

    def main(self):
        down_state_set()
        self.timer['down_settle'] = 1

    def run(self):
        if self.timer['down_settle']:
            return True


class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        return True

    def run(self):
        return True


class MANAGED(GuardState):
    index = 5
    request = True

    def main(self):
        return True

    def run(self):
        return True


class LOCKLOSS(GuardState):
    index = 6
    """Record lockloss events"""
    request = False
    redirect = False

    def main(self):
        down_state_set()
        #TODO, don't apply lockloss counter logic while managed

        # count lock loss
        shared.clf.LOCK_ATTEMPT_COUNTER += 1
        log("LOCK ATTEMPT " + str(shared.clf.LOCK_ATTEMPT_COUNTER))

        if shared.clf.LOCK_ATTEMPT_COUNTER > shared.clf.LOCK_ATTEMPT_MAX:
            shared.clf.LOCK_ATTEMPT_COUNTER = 0
            shared.clf.intervention_msg = "Too Many Lock Failures"
            return 'REQUIRES_INTERVENTION'
        return True


class REQUIRES_INTERVENTION(GuardState):
    index = 7
    request = False
    redirect = False

    def run(self):
        if shared.clf.intervention_msg is not None:
            notify(shared.clf.intervention_msg)
        notify("request non-locking state")
        if ezca['GRD-SQZ_CLF_REQUEST'] in lock_states:
            return False
        else:
            shared.clf.intervention_msg = None
            return True

    
class IDLE(GuardState):
    index = 10

    def run(self):        
        return True



#-------------------------------------------------------------------------------
class LOCKING_CLF_REFL(GuardState):
    index = 20
    request = False

    @fault_checker
    def main(self):

        # Prep analog servo
        ezca['SQZ-CLF_SERVO_IN2GAIN'] = 31 + round(min([-35-ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'],0])) 
        #ezca['SQZ-CLF_SERVO_IN1GAIN'] = 2
        ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
        ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 0

        # Ensure phase is reasonable
        if ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] > 180:
            pass
            #ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = 140
        
        self.timer['done'] = 1
        self.timer['REFL_timeout'] = 1
        self.counter = 0

        # Prep digital servo (additive offset)
        for ii in range(3):
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_%s'%(ii+1)] = 0
        ezca.switch('SQZ-CLF_SERVO_DC','FM1','FM9','FM10','ON','INPUT','FM2','FM3','FM4','FM5','FM6','FM7','FM8','OFF')
        ezca['SQZ-CLF_SERVO_DC_GAIN'] = 1

        
    @fault_checker
    @lock_checker
    def run(self):

        if not self.timer['done']:
            return

        if not shared.clf.REFL_checker():
            if self.timer['REFL_timeout']:
                notify('No CLF beatnote on the REFL PD.')
                ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
                ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 0        
                ezca['SQZ-CLF_SERVO_IN1EN'] = False
                self.counter = 0
                return
        else:
            self.timer['REFL_timeout'] = 1

        # Engage analog servo
        if self.counter == 0:
            ezca['SQZ-CLF_SERVO_IN2EN'] = True
            self.timer['done'] = 1
            self.counter += 1

        # Comm boost not used
        elif self.counter == 1:
            ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
            self.timer['done'] = 1
            self.counter += 1

        # Engage slow boost
        elif self.counter == 2:
            ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 1
            self.timer['done'] = 1
            self.counter += 1

        # Check that CLF locked, if not return to DOWN
        elif self.counter == 3:
            if shared.clf.check_unlocked():
                ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
                ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 0
                notify('Output is railed. Go to down.')
                return 'DOWN'
            else:
                self.counter += 1

        # Send CLF REFL6I to digital DC servo, and turn on DC servo
        elif self.counter == 4:
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_2'] = [-1,1][ezca['SQZ-CLF_SERVO_IN2POL']]
            time.sleep(0.1)
            ezca.switch('SQZ-CLF_SERVO_DC','INPUT','ON')
            self.timer['done'] = 1
            self.counter = 7

        #---This two sections are currently skipped over---
        # Give 30 times more gain
        elif self.counter == 5:
            ezca.switch('SQZ-CLF_SERVO_DC','FM2','ON')
            self.timer['done'] = 1
            self.counter += 1

        # Switch on resg
        elif self.counter == 6:
            ezca.switch('SQZ-CLF_SERVO_DC','FM3','ON')
            self.timer['done'] = 1
            self.counter += 1
        #-------------------------------------------------

        elif self.counter == 7:
            return True

#-------------------------------------------------------------------------------
class CLF_REFL_LOCKED(GuardState):
    index = 50

    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        ezca['SQZ-OPO_TEC_LOCKIN_SWITCH'] = 'Off'
        
    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        shared.clf.LOCK_ATTEMPT_COUNTER = 0
        return True
    
#-------------------------------------------------------------------------------
class LOCKED_and_SLOW(GuardState):
    index = 51
    
    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        # engage excitation
        ezca['SQZ-OPO_TEC_LOCKIN_FREQUENCY'] = 3
        ezca['SQZ-OPO_TEC_LOCKIN_AMPLITUDE'] = 0.03 #0.02
        ezca['SQZ-OPO_TEC_LOCKIN_SWITCH'] = 'On'
        
        # create OPO temperature servo
        self.OPO_TEMP_servo = Servo(ezca, 'SQZ-OPO_TEC_SETTEMP', readback='SQZ-CLF_REFL_RF6_ABS_DEMOD_OUTPUT',
                                    gain=-30, ugf=0.1)
        
        self.servo_sign = [1,-1][ezca['SQZ-CLF_SERVO_IN1POL']]        
        self.timer['wait_settle'] = 20
        self.timer['reset'] = 0
        self.reset = False
        self.thr = 0.001
        self.fail = False
        
    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        servo_sign = [1,-1][ezca['SQZ-CLF_SERVO_IN1POL']]        
        if not servo_sign == self.servo_sign:
            notify('CLF servo sign flipped')
            self.servo_sign = servo_sign
            self.timer['reset'] = 5
            self.reset = True
            self.timer['wait_settle'] = 10

        if self.reset and self.timer['reset']:
            ezca['SQZ-CLF_REFL_RF6_ABS_DEMOD_RSET'] = 2
            self.reset = False

        if self.timer['wait_settle']:
            if not self.fail:
                self.OPO_TEMP_servo.step()
            else:
                notify('TDS might fail. Please check and go to INIT.')
                # reset OPO temperature servo
                self.OPO_TEMP_servo = Servo(ezca, 'SQZ-OPO_TEC_SETTEMP', readback='SQZ-CLF_REFL_RF6_ABS_DEMOD_OUTPUT',
                                            gain=-1000, ugf=0.1)
                
                
            if not 28 < ezca['SQZ-OPO_TEC_SETTEMP'] < 35:
                ezca['SQZ-OPO_TEC_SETTEMP'] = 30.7
                self.fail = True                

            return not self.fail

#-------------------------------------------------------------------------------
class MAXIMIZE_OMCTRANS_Q(GuardState):
    index = 90
    request = False

    @fault_checker
    @lock_checker
    @REFL_checker
    def main(self):
        self.timer['done'] = 0
        self.counter = 0
        for ii in range(3):
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_%s'%(ii+1)] = 0
            
        ezca['SQZ-CLF_SERVO_DC_INMTRX_1_3'] = 1            
        ezca.switch('SQZ-CLF_SERVO_DC','INPUT','OUTPUT','ON','FM1','FM2','FM3','FM4','FM5','FM6','FM7','FM8','FM9','FM10','OFF')
        ezca['SQZ-CLF_SERVO_DC_GAIN'] = 1

        self.tol = 150

    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        if not self.timer['done']:
            return

        if ezca['GRD-SQZ_LO_STATE_N'] < 10:
            notify('LO is not locked')
            return

        if self.counter == 0:
            if ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] > 198:
                self.counter += 2
            else:
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] += 1
                self.timer['done'] = 0.1

        elif self.counter == 1:
            if ezca['SQZ-LO_SERVO_IN1GAIN'] >= -5:
                self.counter += 1
            else:
                ezca['SQZ-LO_SERVO_IN1GAIN'] += 1
                self.timer['done'] = 0.1

        elif self.counter == 2:
            # engage dither
            ezca['SQZ-PHASE_DITHER_OSC_TRAMP'] = 1
            ezca['SQZ-PHASE_DITHER_DEMOD_PHASE'] = -21
            time.sleep(0.1)
            ezca['SQZ-PHASE_DITHER_OSC_CLKGAIN'] = 100
            
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 3:
            ezca.switch('SQZ-PHASE_LOCK','INPUT','ON')
            self.timer['done'] = 2
            self.counter = 6

        elif self.counter == 4:
            self.servo = Servo(ezca, 'SQZ-CLF_REFL_RF6_PHASE_PHASEDEG', readback='SQZ-PHASE_LOCK_OUTPUT', gain = -0.003, ugf=0.1)
            self.timer['in_tol'] = 5            
            self.counter += 1

        elif self.counter == 5:
            if ezca['SQZ-PHASE_LOCK_OUTPUT'] < self.tol:
                if self.timer['in_tol']:
                    self.counter += 1
            else:
                self.timer['in_tol'] = 5
            self.servo.step()

        elif self.counter == 6:
            return True

#-------------------------------------------------------------------------------
class OMCTRANS_Q_MAXIMIZED(GuardState):
    index = 100
    request = True

    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        None
        
    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        return True
        
#-------------------------------------------------------------------------------
class BRING_SQZ_ANGLE_BACK(GuardState):
    index = 110
    request = False

    @lock_checker
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0
        ezca['SQZ-CLF_SERVO_DC_TRAMP'] = 2

    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            ezca.switch('SQZ-PHASE_LOCK','INPUT','OFF')
            self.counter += 1
            self.timer['waiting'] = 1

        elif self.counter == 1:
            ezca['SQZ-PHASE_DITHER_OSC_CLKGAIN'] = 0
            ezca['SQZ-CLF_SERVO_DC_GAIN'] = 0
            self.counter += 1
            self.timer['waiting'] = 2
            
        elif self.counter == 2:
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_3'] = 0
            ezca['SQZ-PHASE_LOCK_RSET'] = 2
            self.counter += 2
            self.timer['waiting'] = 1            

        elif self.counter == 3:
            if ezca['SQZ-LO_SERVO_IN1GAIN'] <= -10:
                self.counter += 1
                ezca['SQZ-LO_SERVO_IN1GAIN'] = -10
            else:
                ezca['SQZ-LO_SERVO_IN1GAIN'] -= 1
                self.timer['waiting'] = 0.3

        elif self.counter == 4:
            if ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] <= 135:
                self.counter += 1
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] = 135
            else:
                ezca['SQZ-CLF_REFL_RF6_PHASE_PHASEDEG'] -= 1
                self.timer['waiting'] = 0.1

        elif self.counter == 5:
            for ii in range(3):
                ezca['SQZ-CLF_SERVO_DC_INMTRX_1_%s'%(ii+1)] = 0
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_2'] = [-1,1][ezca['SQZ-CLF_SERVO_IN2POL']]
            ezca.switch('SQZ-CLF_SERVO_DC','FM1','FM9','FM10','ON','INPUT','FM2','FM3','FM4','FM5','FM6','FM7','FM8','OFF')
            ezca['SQZ-CLF_SERVO_DC_GAIN'] = 1
            self.timer['waiting'] = 2
            self.counter += 1

        elif self.counter == 6:
            ezca.switch('SQZ-CLF_SERVO_DC','INPUT','ON')
            self.timer['waiting'] = 1
            self.counter += 1

        elif self.counter == 7:
            return True


#------------------------------------------------------------------------------------------------------------------
class SWITCH_CLFR_TO_OMCQ(GuardState):
    index = 190
    request = False
    
    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        self.timer['done'] = 0
        self.tol_OMCQ = 40
        self.tol_SUMOUT = 0.04
        self.tol_SERVOMON = 0.01        
        self.counter = 0
        ezca['SQZ-CLF_SERVO_IN1GAIN'] = -10
        self.init_in2gain = ezca['SQZ-CLF_SERVO_IN2GAIN']
        self.target_gain = 7 + round((-40-ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'])/2)
        
        ezca.switch('SQZ-CLF_SUM_OFS','OFFSET','ON')                    

    @fault_checker    
    @lock_checker
    @REFL_checker
    def run(self):
        if not self.timer['done']:
            return

        #----------------------------------------------------------------------------------
        # Changes CLF REFL phase until OMC 3Q matches target
        if self.counter == 0:
            self.servo = Servo(ezca, 'SQZ-CLF_REFL_RF6_PHASE_PHASEDEG', readback='SQZ-OMC_TRANS_RF3_Q_MON',
                               setpoint=ezca['SQZ-OMC_TRANS_RF3_Q_TARGET'],gain=0.3, ugf=0.1)
            self.timer['in_tol'] = 3
            self.counter += 1

        elif self.counter == 1:
            notify('Moving CLF REFL phase to bring OMC 3Q to target')
            if abs(ezca['SQZ-OMC_TRANS_RF3_Q_MON']-ezca['SQZ-OMC_TRANS_RF3_Q_TARGET']) < self.tol_OMCQ:
                if self.timer['in_tol']:
                    self.counter += 1
            else:
                self.timer['in_tol'] = 3
            self.servo.step()
        # FIXME: what if on wrong slope, this servo will just run away
        #----------------------------------------------------------------------------------

        #----------------------------------------------------------------------------------
        # Change offset (summed to analog OMC-3Q signal in summing node) until sum equals zero    
        elif self.counter == 2:
            self.servo2 = Servo(ezca, 'SQZ-CLF_SUM_OFS_OFFSET', readback='SQZ-CLF_SUM_OUT_OUTPUT',
                                gain=1, ugf=1)
            self.timer['in_tol'] = 3
            self.timer['timeout'] = 30
            self.counter += 1
            ezca['SQZ-CLF_SUM_OFS_TRAMP'] = 0.05
            
        elif self.counter == 3:
            notify('Set offset to match current position')            
            if abs(ezca['SQZ-CLF_SUM_OUT_OUTPUT']) < self.tol_SUMOUT:                
                if self.timer['in_tol']:
                    self.counter += 1
            else:
                if self.timer['timeout']:
                    self.counter += 1
                self.timer['in_tol'] = 3
            self.servo2.step()
        #----------------------------------------------------------------------------------

        # Set CLF-REFL-6I error to additive offset(?) component to zero
        elif self.counter == 4:
            notify('Disable DC loop')
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_2'] = 0
            self.timer['done'] = 0.5
            self.counter += 1

        # Feedback OMC-3Q analog signal (in1 gain should be -10)        
        elif self.counter == 5:
            notify('Engage OMCQ.')
            ezca['SQZ-CLF_SERVO_IN1EN'] = 'On'
            self.timer['done'] = 1
            self.counter += 1

        # Increase OMC-3Q (in1) gain up to 0dB 
        elif self.counter == 6:
            notify('Increasing IN1GAIN')
            if ezca['SQZ-CLF_SERVO_IN1GAIN'] == self.target_gain:
                self.counter += 1
            else:
                ezca['SQZ-CLF_SERVO_IN1GAIN'] += 1
                self.timer['done'] = 0.4             

        # Boost OMC-3Q (in1) gain up to 13dB
        elif self.counter == 7:
            notify('Decreasing IN2GAIN')            
            if ezca['SQZ-CLF_SERVO_IN2GAIN'] < max([-30,self.init_in2gain-20]):
                self.counter += 1
                self.timer['done'] = 0.4      
            else:
                ezca['SQZ-CLF_SERVO_IN2GAIN'] -= 1

        # Turn off CLF-REFL-61 analog feedback
        elif self.counter == 8:
            notify('Turning off CLF 2f.')                        
            ezca['SQZ-CLF_SERVO_IN2EN'] = 'Off'
            self.timer['done'] = 1
            self.counter += 1

        # Engage OMC-3Q additive offset
        elif self.counter == 9:
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_1'] = 1
            self.counter += 1

        # Change offset (summed to analog OMC-3Q signal in summing node) until L1:SQZ-CLF_SERVO_SUMMON to zero
        elif self.counter == 10:
            self.servo3 = Servo(ezca, 'SQZ-CLF_SUM_OFS_OFFSET', readback='SQZ-CLF_SERVO_SUMMON',
                                gain=1, ugf=0.5)
            self.timer['in_tol'] = 3
            self.timer['timeout'] = 30
            self.counter += 1
            ezca['SQZ-CLF_SUM_OFS_TRAMP'] = 0.05
            
        elif self.counter == 11:
            notify('Set offset to match current position')            
            if abs(ezca['SQZ-CLF_SUM_OUT_OUTPUT']) < self.tol_SERVOMON:                
                if self.timer['in_tol']:
                    self.counter += 1
            else:
                if self.timer['timeout']:
                    self.counter += 1
                self.timer['in_tol'] = 3
            self.servo3.step()
                        
        elif self.counter == 12:        
            return True


#------------------------------------------------------------------------------------------------------------------
class OMCQ_LOCKED(GuardState):
    index = 200

    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        ezca['SQZ-OPO_TEC_LOCKIN_SWITCH'] = 'Off'
        
    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        shared.clf.LOCK_ATTEMPT_COUNTER = 0
        return True
    
#------------------------------------------------------------------------------------------------------------------
class SWITCH_OMCQ_TO_CLFR(GuardState):
    index = 210
    request = False
    
    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        self.timer['done'] = 0.5
        self.counter = 0

        
        self.init_in1gain = ezca['SQZ-CLF_SERVO_IN1GAIN'] 
        self.target_in2gain = min([31, 31 + round(-35-ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON'])])
        ezca['SQZ-CLF_SERVO_IN2GAIN'] = self.target_in2gain-20

    @fault_checker    
    @lock_checker
    @REFL_checker
    def run(self):
        if not self.timer['done']:            
            return

        #--------------------------------------------------------
        # Switch DC off
        if self.counter == 0:            
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_1'] = 0
            self.timer['done'] = 1
            self.counter += 1

        # Switch CLF REFL back on
        elif self.counter == 1:
            ezca['SQZ-CLF_SERVO_IN2EN'] = 'On'
            self.timer['done'] = 1
            self.counter += 1

        # Increase CLF REFL (in2) gain up to target
        elif self.counter == 2:
            notify('Increasing IN2GAIN')
            if ezca['SQZ-CLF_SERVO_IN2GAIN'] > min([30,self.target_in2gain]):
                self.counter += 1
            else:
                ezca['SQZ-CLF_SERVO_IN2GAIN'] += 1
                self.timer['done'] = 0.3            

        # Decrease OMC-3Q (in1) gain 
        elif self.counter == 3:
            notify('Decreasing IN1GAIN')
            if ezca['SQZ-CLF_SERVO_IN1GAIN'] < max([-31,self.init_in1gain - 20]):
                self.counter += 1
            else:
                ezca['SQZ-CLF_SERVO_IN1GAIN'] -= 1
                self.timer['done'] = 0.3           
                
        elif self.counter == 4:
            ezca['SQZ-CLF_SERVO_IN1EN'] = 'Off'
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 5:
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_2'] = 1
            self.counter += 1

        elif self.counter == 6:
            return True
#------------------------------------------------------------------------------------------------------------------
class MIX_OMCQ(GuardState):
    index = 390
    request = False
    
    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        self.timer['done'] = 0
        self.counter = 0


    @fault_checker    
    @lock_checker
    @REFL_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            # switch CLF DC
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_2'] = 0
            time.sleep(0.3)
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_1'] = 1
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 1:
            # gain up
            ezca.switch('SQZ-CLF_SERVO_DC','FM4','ON')
            self.timer['done'] = 3
            self.counter += 1

        elif self.counter == 2:
            return True
#------------------------------------------------------------------------------------------------------------------
class OMCQ_DC_LOCKED(GuardState):
    index = 400

    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        ezca['SQZ-OPO_TEC_LOCKIN_SWITCH'] = 'Off'
        
    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        shared.clf.LOCK_ATTEMPT_COUNTER = 0
        return True
    
#------------------------------------------------------------------------------------------------------------------
class REMOVE_OMCQ(GuardState):
    index = 410
    request = False
    
    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        self.timer['done'] = 0
        self.counter = 0
        self.tolerance = 1 #5e-4


    @fault_checker    
    @lock_checker
    @REFL_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            # adjust sqz angle
            log('Setting CLFANG cds servo')
            # create servo
            self.servo = Servo(ezca, 'SQZ-CLF_REFL_RF6_PHASE_PHASEDEG',
                                         readback='SQZ-CLF_REFL_RF6_I_MON',
                                         gain=-3, ugf=3)
            self.timer['in_Tol'] = 2         
            self.counter += 1

        elif self.counter == 1:
            self.servo.step()
            
            if abs(ezca['SQZ-CLF_REFL_RF6_I_MON']) < self.tolerance:
                if self.timer['in_Tol']:                    
                    self.counter += 1
            else:
                self.timer['in_Tol'] = 2
                
        elif self.counter == 2:
            # gain down
            ezca.switch('SQZ-CLF_SERVO_DC','FM4','OFF')
            self.timer['done'] = 3
            self.counter += 1

        if self.counter == 3:
            # switch CLF DC
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_1'] = 0
            time.sleep(0.3)
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_2'] = 1
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 4:
            return True

            
#-------------------------------------------------------------------------------
class LOCKING_SINGLE_CLF(GuardState):
    index = 300
    request = False

    def local_down(self):
        ezca['SQZ-CLF_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
        ezca['SQZ-CLF_SERVO_IN1GAIN'] = 6
        
    @fault_checker    
    @lock_checker
    def main(self):
        ezca.switch('SQZ-CLF_SUM_OFS','OFFSET','OFF')
        ezca.switch('SQZ-CLF_SERVO_DC','INPUT','OFF','OFFSET','FM1','FM2','FM9','ON')
        ezca['SQZ-CLF_SERVO_DC_TRAMP'] = 0.5
        ezca['SQZ-CLF_SERVO_DC_OFFSET'] = ezca['SQZ-OMC_TRANS_RF3_Q_TARGET']
        
        ezca['SQZ-CLF_SERVO_IN1GAIN'] = 6
        self.counter = 0
        self.timer['done'] = 0
        for ii in range(3):
            ezca['SQZ-CLF_SERVO_DC_INMTRX_1_%d'%(ii+1)] = 0
        ezca['SQZ-CLF_SERVO_DC_INMTRX_1_1'] = 1

        
        

    @fault_checker    
    @lock_checker
    def run(self):
        if not self.timer['done']:
            return

        if not shared.clf.OPO_CLFlock_checker():
            notify('waiting for OPO to lock on CLF')
            self.local_down()
            self.counter = 0
            return

        if self.counter == 0:
            ezca['SQZ-CLF_SERVO_IN1EN'] = 'On'
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 1:
            ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 'On'            
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 2:
            ezca.switch('SQZ-CLF_SERVO_DC','INPUT','ON')
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 3:
            ezca.switch('SQZ-CLF_SERVO_DC','FM3','ON')
            self.timer['done'] = 3
            self.counter += 1
            
        else:
            return True

                                 
#-------------------------------------------------------------------------------
class ENGAGING_SINGLE_CLF_ADS(GuardState):
    index = 501
    request = False

    def inmtrx(self):
        ZM4to1 = {}
        ZM4to2 = {}
        ZM6to1 = {}
        ZM6to2 = {}
        
        ZM4to1['P'] = -2.38/50
        ZM4to2['P'] = 0.332/50
        ZM6to1['P'] = 0.590/50    
        ZM6to2['P'] = -0.435/50    

        ZM4to1['Y'] = -0.557/50
        ZM4to2['Y'] = 1.81/50
        ZM6to1['Y'] = 0.464/50
        ZM6to2['Y'] = -3.73/50

        for dof in ['P','Y']:
            sensmtrx = np.matrix([[ZM4to1[dof],ZM6to1[dof]],[ZM4to2[dof],ZM6to2[dof]]])
            inmtrx = np.linalg.inv(sensmtrx)

            ezca['SQZ-ASC_INMATRIX_%s_1_8'%dof] = inmtrx[0,0]
            ezca['SQZ-ASC_INMATRIX_%s_1_9'%dof] = inmtrx[0,1]
            ezca['SQZ-ASC_INMATRIX_%s_2_8'%dof] = inmtrx[1,0]
            ezca['SQZ-ASC_INMATRIX_%s_2_9'%dof] = inmtrx[1,1]

    @CLF_DITHER_LOCK_checker            
    @fault_checker    
    @lock_checker
    def main(self):
        self.timer['waiting'] = 3
        self.counter = 0

        for ii in range(4):
            ezca['SQZ-ASC_LO_INMTRX_1_%d'%(ii+1)] = 0
        ezca['SQZ-ASC_LO_INMTRX_1_1'] = 1
        
        for dof1 in ['PIT','YAW']:
            for ii in range(2):
                ezca['SQZ-ASC_ADS_%s%d_DEMOD_PHASE'%(dof1,ii+1)] = 0
                
        for dof in ['P','Y']:
            for ii in range(2):
                for jj in range(11):
                    ezca['SQZ-ASC_INMATRIX_%s_%d_%d'%(dof,ii+1,jj+1)] = 0

        self.inmtrx()
                

    @fault_checker    
    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            # engage dither
            gainlist = {'PIT1':1000,'PIT2':1000,'YAW1':1000,'YAW2':1000}
            freqlist = {'PIT1':15,'PIT2':18,'YAW1':12,'YAW2':9}
            for dof in ['PIT1','PIT2','YAW1','YAW2']:
                ezca['SQZ-ASC_ADS_%s_OSC_FREQ'%dof] = freqlist[dof]                
                ezca['SQZ-ASC_ADS_%s_OSC_TRAMP'%dof] = 3
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = gainlist[dof]
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1                
            self.timer['waiting'] = 10
            self.counter += 1
            
        elif self.counter == 1:
            if ezca['SQZ-ASC_WFS_GAIN'] >= 1:
                ezca['SQZ-ASC_WFS_GAIN'] = 1
                self.counter += 1
                self.timer['waiting'] = 1
            else:                
                ezca['SQZ-ASC_WFS_GAIN'] += 0.1
                self.timer['waiting'] = 0.3
        
        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class DISABLE_CLF_ADS(GuardState):
    index = 503
    request = False
    
    @CLF_DITHER_LOCK_checker            
    @fault_checker    
    @lock_checker
    def main(self):
        self.timer['done'] = 0
        self.counter = 0

        for dof in ['PIT','YAW']:
            for ii in range(2):
                ezca['SQZ-ASC_ADS_%s%d_OSC_TRAMP'%(dof,ii+1)] = 3

    @CLF_DITHER_LOCK_checker            
    @fault_checker    
    @lock_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:            
            ezca['SQZ-ASC_WFS_GAIN'] = 0
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 1:
            for dof in ['PIT','YAW']:
                for ii in range(2):
                    ezca['SQZ-ASC_ADS_%s%d_OSC_CLKGAIN'%(dof,ii+1)] = 0

            self.timer['done'] = 3
            self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class ENGAGING_SINGLE_ADF_ADS(GuardState):
    index = 502
    request = False

    def inmtrx(self):
        ZM4to1 = {}
        ZM4to2 = {}
        ZM6to1 = {}
        ZM6to2 = {}
        
        ZM4to1['P'] = -0.632/100
        ZM4to2['P'] = 1.86/100
        ZM6to1['P'] = 0/100    
        ZM6to2['P'] = -2.01/100    

        ZM4to1['Y'] = -3.04/100
        ZM4to2['Y'] = 0.57/100
        ZM6to1['Y'] = 2.9/100
        ZM6to2['Y'] = -1.08/100

        for dof in ['Y','P']:
            sensmtrx = np.matrix([[ZM4to1[dof],ZM6to1[dof]],[ZM4to2[dof],ZM6to2[dof]]])
            inmtrx = np.linalg.inv(sensmtrx)

            ezca['SQZ-ASC_INMATRIX_%s_1_8'%dof] = inmtrx[0,0]
            ezca['SQZ-ASC_INMATRIX_%s_1_9'%dof] = inmtrx[0,1]
            ezca['SQZ-ASC_INMATRIX_%s_2_8'%dof] = inmtrx[1,0]
            ezca['SQZ-ASC_INMATRIX_%s_2_9'%dof] = inmtrx[1,1]

    @CLF_DITHER_LOCK_checker            
    @fault_checker    
    @lock_checker
    def main(self):
        self.timer['waiting'] = 3
        self.counter = 0

        for ii in range(4):
            ezca['SQZ-ASC_LO_INMTRX_1_%d'%(ii+1)] = 0
        ezca['SQZ-ASC_LO_INMTRX_1_3'] = 1
        
        for dof in ['P','Y']:
            for ii in range(2):
                for jj in range(11):
                    ezca['SQZ-ASC_INMATRIX_%s_%d_%d'%(dof,ii+1,jj+1)] = 0

        self.inmtrx()
                

    @fault_checker    
    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            ezca['SQZ-RLF_INTEGRATION_ADFEN'] = 'On'
            for iq in ['I','Q']:
                filt = ezca.get_LIGOFilter('SQZ-ADF_OMC_TRANS_DEMOD_%s'%iq)
                filt.only_on('FM5','FM6','INPUT','OUTPUT')
            self.timer['waiting'] = 3
            self.counter += 1

        elif self.counter == 1:                    
            # engage dither
            gainlist = {'PIT1':600,'PIT2':600,'YAW1':600,'YAW2':600}
            freqlist = {'PIT1':8,'PIT2':7,'YAW1':5,'YAW2':6}
            phaselist = {'PIT1':0,'PIT2':0,'YAW1':0,'YAW2':0}
            for dof in ['PIT1','PIT2','YAW1','YAW2']:
                ezca['SQZ-ASC_ADS_%s_DEMOD_PHASE'%(dof)] = phaselist[dof]
                ezca['SQZ-ASC_ADS_%s_OSC_FREQ'%dof] = freqlist[dof]

                ezca['SQZ-ASC_ADS_%s_OSC_TRAMP'%dof] = 3
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = gainlist[dof]                
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1

            self.timer['waiting'] = 10
            self.counter += 1
            
        elif self.counter == 2:
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.03:
                ezca['SQZ-ASC_WFS_GAIN'] = 0.03
                self.counter += 1
                self.timer['waiting'] = 1
            else:                
                ezca['SQZ-ASC_WFS_GAIN'] += 0.01
                self.timer['waiting'] = 0.2
        
        elif self.counter == 3:
            return True

#-------------------------------------------------------------------------------
class WAITING_ADS(GuardState):
    index = 850
    request = False

    DOFLIST = ['POS','ANG']
    
    def popappend(self,nplist,value):
        if nplist[-1] == value:
            return nplist
        else:
            return np.append(nplist[1:],value)

    @lock_checker
    def main(self):        
        self.counter = 0

        self.timer['done'] = 0
        self.checktime = 10


        self.dataT = 15 # sec
        self.timer['data_acq'] = self.dataT+1
        
        self.dataset = {}
        for dof1 in ['POS','ANG']:
            for dof2 in ['P','Y']:            
                self.dataset[dof1+dof2] = np.zeros(self.dataT*16)

    @lock_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            for dof1 in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    channame = 'SQZ-ASC_%s_%s'%(dof1,dof2)
                    filt = ezca.get_LIGOFilter(channame)
                    self.dataset[dof1+dof2] = self.popappend(self.dataset[dof1+dof2],ezca['%s_INMON'%(channame)]+ezca['%s_OFFSET'%(channame)]*filt.is_on('OFFSET'))

            if not self.timer['data_acq']:
                self.timer['ASC_zero'] = self.checktime
                notify('waiting for getdata')
                return

            ASC_done = True
            msg = 'waiting for '
            for dof1 in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    dataset = self.dataset[dof1+dof2]
                    if abs(np.mean(dataset)) > np.std(dataset)/3:
                        ASC_done = False
                        msg += dof1 + ' ' + dof2 + ', '


            if ASC_done:
                if self.timer['ASC_zero']:
                    ezca['SQZ-ASC_WFS_GAIN'] = 0.2
                    self.timer['settle'] = 60
                    self.counter += 1

            else:
                notify(msg)
                self.timer['ASC_zero'] = self.checktime
                return

        elif self.counter == 1:
            if self.timer['settle']:
                return True
            else:
                notify('waiting to settle.')
            

#-------------------------------------------------------------------------------
class SINGLE_CLF_ALIGNED(GuardState):
    index = 900

    @fault_checker
    @lock_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
class SINGLE_ADF_ALIGNED(GuardState):
    index = 901

    @fault_checker
    @lock_checker
    def run(self):
        return True
    
#-------------------------------------------------------------------------------
class LOCKED(GuardState):
    index = 1000

    @fault_checker    
    @lock_checker
    @REFL_checker
    def main(self):
        ezca['SQZ-OPO_TEC_LOCKIN_SWITCH'] = 'Off'
        
    @fault_checker
    @lock_checker
    @REFL_checker
    def run(self):
        shared.clf.LOCK_ATTEMPT_COUNTER = 0
        return True
        
##################################################

edges = [
    ('INIT', 'DOWN'),
    ('LOCKLOSS', 'DOWN'),
    ('REQUIRES_INTERVENTION', 'DOWN'),
    ('FAULT', 'DOWN'),
    
    ('DOWN', 'IDLE'),
    #('IDLE','LOCKING_OMCQ'),
    #('LOCKING_OMCQ','OMCQ_LOCKED'),

    #('LOCKED','OMCQ_LOCKED')
    
    ('IDLE','LOCKING_CLF_REFL'),    
    ('LOCKING_CLF_REFL','CLF_REFL_LOCKED'),
    ('CLF_REFL_LOCKED','LOCKED_and_SLOW'),
    ('LOCKED_and_SLOW','CLF_REFL_LOCKED'),
    ('CLF_REFL_LOCKED','MIX_OMCQ'),
    ('MIX_OMCQ','OMCQ_DC_LOCKED'),
    ('OMCQ_LOCKED','LOCKED'),
    ('LOCKED','OMCQ_LOCKED'),
    ('OMCQ_DC_LOCKED','REMOVE_OMCQ'),
    ('REMOVE_OMCQ','CLF_REFL_LOCKED'),

    
    ('CLF_REFL_LOCKED','SWITCH_CLFR_TO_OMCQ'),
    ('SWITCH_CLFR_TO_OMCQ','OMCQ_LOCKED'),
    ('OMCQ_LOCKED','SWITCH_OMCQ_TO_CLFR'),
    ('SWITCH_OMCQ_TO_CLFR','CLF_REFL_LOCKED'),

    ('CLF_REFL_LOCKED','MAXIMIZE_OMCTRANS_Q'),
    ('MAXIMIZE_OMCTRANS_Q','OMCTRANS_Q_MAXIMIZED'),    
    ('OMCTRANS_Q_MAXIMIZED','BRING_SQZ_ANGLE_BACK'),
    ('BRING_SQZ_ANGLE_BACK','CLF_REFL_LOCKED'),

    ('IDLE','LOCKING_SINGLE_CLF'),
    ('LOCKING_SINGLE_CLF','ENGAGING_SINGLE_CLF_ADS'),
    ('ENGAGING_SINGLE_CLF_ADS','WAITING_ADS',2),
    ('WAITING_ADS','SINGLE_CLF_ALIGNED'),
    ('SINGLE_CLF_ALIGNED','DISABLE_CLF_ADS'),
    ('DISABLE_CLF_ADS','ENGAGING_SINGLE_ADF_ADS'),
    ('ENGAGING_SINGLE_ADF_ADS','SINGLE_ADF_ALIGNED'),
    

    #('LOCKING_SINGLE_CLF','ENGAGING_SINGLE_ADF_ADS'),
    #('ENGAGING_SINGLE_ADF_ADS','WAITING_ADS',1),
    #('WAITING_ADS','SINGLE_ADF_ALIGNED'),

]

#list of states which can fail out too many times and freeze in LOCKLOSS_TOOMANY
lock_states = [
    'LOCKING', 'LOCKED',
    
]

'''
class LOCKING_DIGITAL(GuardState):
    index = 26
    request = False

    @fault_checker
    def main(self):
        ezca['SQZ-CLF_SERVO_IN2GAIN'] = -20
        
        ezca['SQZ-CLF_SERVO_COMBOOST'] = 0
        ezca['SQZ-CLF_SERVO_SLOWBOOST'] = 0        
        self.timer['done'] = 1
        self.timer['REFL_timeout'] = 1
        self.counter = 0
        
    @fault_checker
    @lock_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            ezca['SQZ-CLF_SERVO_IN2EN'] = True
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 1:
            ezca.switch('SQZ-CLF_SERVO_DC','FM2','ON')
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 2:
            ezca.switch('SQZ-CLF_SERVO_DC','FM3','ON')
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 3:
            if shared.clf.check_unlocked():
                ezca['SQZ-CLF_SERVO_IN2EN'] = False
                notify('Output is railed. Go to down.')
                return 'DOWN'
            else:
                return True
            
'''

