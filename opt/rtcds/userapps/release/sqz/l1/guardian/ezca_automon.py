from ezca import (Ezca, EzcaConnectError, EzcaError)
import contextlib

from guardian.state import (
    TimerManager,
)

SPM_MOMENTARY_SUFFIXES = [
    '_RSET',
    '_LOAD_MATRIX',
]


channel_hints_logpv       = dict()
channel_hints_monitor     = dict()
channel_hints_write_wait  = dict()
channel_hints_cache_write = dict()
channel_hints_cache_read  = dict()


class EzcaEpochAutomon(Ezca):
    """LIGO EPICS channel access interface.

    Ezca is a LIGO-specific wrapper around the EPICS channel access
    library.  It is designed to more easily handle the specific form
    of LIGO EPICS channel names, and the parameters they expose.  It
    provides standard channel read() and write() methods, as well as
    LIGO-specific methods, such as for interacting with standard
    filter modules (SFM) (see the LIGOFilter class).

    A channel access prefix can be specified for all channel access
    calls using the 'prefix' and 'ifo' arguments.  See
    ezca.parse_ifo_prefix for information on how the ultimate channel
    access prefix is chosen from 'prefix' and 'ifo'.  If 'ifo' is not
    specified, the value of the 'IFO' environment variable will be
    used.  If no prefix at all is desired, even if the IFO environment
    is set, specify ifo=None (prefix=None by default) e.g.:

    >>> ezca.Ezca(ifo=None)
    Ezca(prefix='')

    'logger' is a logging object that will be used to log channel
    writes.

    """

    @classmethod
    def monkeypatch_subclass_instance(cls, self):
        """
        Run after patching a live ezca instance to finish setting up the type changes
        """
        #mapping pvname -> (value, args, kwargs) where the args, kwargs are further things given to "write"
        self.__class__ = cls
        self._pv_write_cache = dict()
        self._storing_interval = False
        self._pv2channel = dict()

    #MONKEYPATCH CHANGES
    def epoch_commit(self, storing = False):
        #SHOULD BATCH CALL!
        for pv, (value, args, kwargs) in self._pv_write_cache.items():
            #flag that the value was just cached and not written
            if args is None:
                continue
            self._write(pv, value, *args, **kwargs)
        self._pv_write_cache.clear()
        self._storing_interval = storing

    @contextlib.contextmanager
    def cache_context(self):
        prev_store = self._storing_interval
        if prev_store is False:
            self.epoch_commit(storing = True)
        yield
        if prev_store is False:
            self.epoch_commit(storing = False)
        return

    def deco_autocommit(self, func):
        """
        Should be the outermost wrapper of a guardian state
        """
        def fwrap(*args, **kwargs):
            with self.cache_context():
                return func(*args, **kwargs)

        fwrap.__name__ = func.__name__
        fwrap.__doc__  = func.__doc__
        return fwrap

    def read(self, channel, *args, **kwargs):
        """Read channel value.

        See connect() for more info.

        """
        cached_val = self._pv_write_cache.get(channel, None)
        if cached_val is None:
            value = super(EzcaEpochAutomon, self).read(channel, *args, **kwargs)
            if self._storing_interval:
                if channel_hints_cache_read.get(channel, True):
                    self._pv_write_cache[channel] = (value, None, None)
        else:
            value = cached_val[0]
        return value

    def write(self, channel, value, *args, **kwargs):
        """Write channel value.

        All writes are recorded in a setpoint cache.  See
        check_setpoints() for checking current settings against set
        points.

        See connect() for more info.

        """
        if self._storing_interval and channel_hints_cache_write.get(channel, True):
            self._pv_write_cache[channel] = (value, args, kwargs)
        else:
            try:
                del self._pv_write_cache[channel]
            except KeyError:
                pass
            return self._write(channel, value, *args, **kwargs)

    def _write(self, channel, value, *args, **kwargs):
        try:
            kwargs.setdefault('wait', channel_hints_write_wait.get(channel, True))
            kwargs.setdefault('monitor', channel_hints_monitor.get(channel, True))
        except AttributeError:
            pass
        return super(EzcaEpochAutomon, self).write(channel, value, *args, **kwargs)

    def pv2channel(self, pv):
        channel = self._pv2channel.get(id(pv), None)
        if channel is None:
            for channel, other_pv in self.pvs.items():
                if pv is other_pv:
                    break
            else:  # only entered on py for loops if break not called
                return None
            self._pv2channel[id(pv)] = channel
        return channel

    def _logput(self, pv, value=None):
        if value is None:
            value = pv.get()
        message = "%s => %s" % (pv.pvname, value)
        channel = self.pv2channel(pv)
        logpv = channel_hints_logpv.get(channel, True)
        if logpv:
            self._log.warning(message)


def deco_autocommit(func):
    """
    Should be the outermost wrapper of a guardian state
    """
    def fwrap(*args, **kwargs):
        """
        Protect against non-patched instances, then run the code with the cache context.
        """
        try:
            cache_context = ezca.cache_context
        except AttributeError:
            EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
            cache_context = ezca.cache_context

        with cache_context():
            return func(*args, **kwargs)

    fwrap.__name__ = func.__name__
    fwrap.__doc__  = func.__doc__
    return fwrap


#this is to keep emacs python-mode happy from lint errors due to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None


class EzcaAccessor(object):
    """
    Python "descriptor" type that forwards access through a stored ezca reference. It also allows setting up connection preferences it initialization
    """
    def __init__(
            self,
            pvname,
            logpv       = None,
            monitor     = None,
            write_wait  = None,
            cache_write = None,
            cache_read  = None,
    ):
        #some formatting options to generate PV's from templates
        self.pvname = pvname.format(
            IFO = IFO,
        )

        if logpv is not None:
            channel_hints_logpv[self.pvname] = logpv

        if monitor is not None:
            channel_hints_monitor[self.pvname] = monitor

        if write_wait is not None:
            channel_hints_write_wait[self.pvname] = write_wait

        if cache_write is not None:
            channel_hints_cache_write[self.pvname] = cache_write

        if cache_read is not None:
            channel_hints_cache_read[self.pvname] = cache_read

    def __get__(self, obj, otype = None):
        return ezca[self.pvname]

    def __set__(self, obj, value):
        ezca[self.pvname] = value


class SharedState(object):
    pass


class DecoShared(object):
    """
    Decorator to construct objects when first accessed. Using a non-data descriptor layout (no __set__ or __del__ methods), this has NO runtime penalty
    """
    def __init__(self, construct_func):
        self.construct_func = construct_func
        self.__name__ = construct_func.__name__
        self.__doc__  = construct_func.__doc__

    def __get__(self, obj, otype = None):
        if obj is not None:
            inst = self.construct_func(obj)
            obj.__dict__[self.__name__] = inst
            return inst
        else:
            return self.construct_func

class EzcaUser(object):
    """
    has the necessary extra stuff to initialize classes using EzcaAccessor objects
    """
    pass
